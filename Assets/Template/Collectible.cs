﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.GetComponent<Jumper>())
        {
            Destroy(gameObject);
        }
    }
}
