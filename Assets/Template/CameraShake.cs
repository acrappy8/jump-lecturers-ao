﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shake;

    public float shakeReturnSpeed = 8.0f;
    public float cameraReturnSpeed = 16.0f;

    void Update()
    {
        float x = Random.Range(-1.0f, 1.0f);
        float y = Random.Range(-1.0f, 1.0f);

        transform.position += new Vector3(x, y, 0.0f) * shake;

        shake = Mathf.Lerp(shake, 0.0f, shakeReturnSpeed * Time.deltaTime);

        transform.position = Vector3.Lerp(transform.position, new Vector3(0.0f, 0.0f, -10.0f), cameraReturnSpeed * Time.deltaTime);
    }
}
