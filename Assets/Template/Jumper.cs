﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : MonoBehaviour
{
    [Header("Jumping")]

    [Tooltip("The speed that we travel when we jump")] 
    public float jumpStrength = 16.0f;

    public float cooldownLength = 2.0f;
    public float cooldownTimer = 0.0f;

    public Color readyColour;
    public Color notReadyColour;

    public SpriteRenderer display;

    public CameraShake shake;

    private void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 0.0f;

        Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
        targetPos.z = transform.position.z;

        Vector3 targetDir = (targetPos - transform.position).normalized;

        cooldownTimer -= Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && cooldownTimer < 0.0f)
        {
            shake.shake += 1.0f;
            GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * jumpStrength;
            cooldownTimer = cooldownLength;
        }

        if (cooldownTimer < 0.0f)
        {
            display.color = readyColour;
        }
        else
        {
            display.color = notReadyColour;
        }
    }
}
